// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const LOGIN_URL = "https://huaweiincentives.foneworx.co.za/productincentive/rest/v1/api#/login";
export const LOGIN_KEY = "f122491c88101ce047e40b760300ac33076344b6df36d93686918f60";
export const API_URL = "https://huaweiincentives.foneworx.co.za/productincentive/rest/v1";
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

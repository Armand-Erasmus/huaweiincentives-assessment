import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { API_URL } from "src/environments/environment";
import { DOCUMENT } from "@angular/common";
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  responseBody: any;
  errorMessage: string;

  constructor(
    private http: HttpClient,
    @Inject(DOCUMENT) private _document: Document,
    private router: Router
  ) {}

  ngOnInit() {
    this.http.get<any>(API_URL).subscribe({
        next: data => {
            this.responseBody = data;
        },
        error: error => {
            this.errorMessage = error.message;
            console.error('There was an error!', error);
        }
    })
  }

  ionRefresh() {
    this._document.defaultView.location.reload();
  }

  logOut() {
    this._document.defaultView.localStorage.clear();

    this.router.navigateByUrl("/login");
  }

}

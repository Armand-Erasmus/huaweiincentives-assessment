import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { LOGIN_URL, LOGIN_KEY } from "src/environments/environment";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  error: string = "";

  loading = false;
  submitted = false;

  WRONG_CREDS_ERROR_WORDING = "Incorrect username or password! Please enter a valid username and password combination";

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  ngOnInit() {
    console.log ('*** LoginPage ngOnInit hook ***');  
  }

  ngOnDestroy() {
    console.log ('*** LoginPage.ngOnDestroy hook ***'); 
  }

  login(form) {
    this.http.post<JSON>(LOGIN_URL, LOGIN_KEY, form).subscribe({
        next: data => {
          console.log(data);
            this.router.navigateByUrl("/home");
        },
        error: error => {
            this.WRONG_CREDS_ERROR_WORDING = error.message;
            console.error('There was an error!', error);

            //Adding routing to home regardless due to odd behaviour on post to login endpoint
            this.router.navigateByUrl("/home");
        }
    })
  }
}
